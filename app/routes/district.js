import Route from '@ember/routing/route';
import {cityData, districtData} from './data';

export default class CountryRoute extends Route {
  async model({district_id}) {
    let district = districtData.filter(count => (count.id === +district_id))
    cityData.forEach((item, i) => {
      item['name'] = item.city_name;
    });

    if (district.length > 0)
    return {
      model: district[0],
      list: cityData
    };
  }
}
