import Route from '@ember/routing/route';
import {districtData} from './data';

export default class CountryListRoute extends Route {
  async model() {
    districtData.forEach( c => c.name = c.district_name );
    return districtData;
  }
}
