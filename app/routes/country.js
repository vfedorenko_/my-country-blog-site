import Route from '@ember/routing/route';
import {countryData, districtData} from './data';

export default class CountryRoute extends Route {
  async model({country_id}) {
    let country = countryData.filter(count => (count.id === +country_id))
    districtData.forEach((item, i) => {
      item['name'] = item.district_name;
    });

    console.log(country[0]);
    if (country.length > 0) return { model: country[0], list: districtData};
  }
}
