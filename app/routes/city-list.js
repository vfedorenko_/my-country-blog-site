import Route from '@ember/routing/route';
import {cityData} from './data';

export default class CountryListRoute extends Route {
  async model() {
    cityData.forEach( c => {
      c.name = c.city_name;
      c.bgImage = c.images[Math.random(c.images.length)];
      console.log(c.bgImage)
    } );
    return cityData;
  }
}
