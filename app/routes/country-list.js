import Route from '@ember/routing/route';
import {countryData} from './data';

export default class CountryListRoute extends Route {
  async model() {
    countryData.forEach( c => c.name = c.country_name );
    return countryData;
  }
}
