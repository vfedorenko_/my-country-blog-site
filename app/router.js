import EmberRouter from '@ember/routing/router';
import config from 'where-to-travel/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function() {
  this.route('index', { path: '/' });
  this.route('country', { path: '/country/:country_id' });
  this.route('countryList', {path: '/countries'});
  this.route('district', { path: '/district/:district_id' });
  this.route('districtList', { path: '/districts' });
  this.route('city', { path: '/city/:city_id' });
  this.route('cityList', { path: '/cities' });
  this.route('contact');
  this.route('notFound', { path: '/*path' });
});
