import Component from "@glimmer/component";
import { action } from "@ember/object";

export default class ItemListComponent extends Component {
  @action
  showItem(item) {
    alert(`The item's name is ${item}!`);
  }
}
