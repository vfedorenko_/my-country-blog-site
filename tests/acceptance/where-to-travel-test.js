import { module, test } from 'qunit';
import { click, visit, currentURL } from '@ember/test-helpers';
import { setupApplicationTest } from 'ember-qunit';

module('Acceptance | where to travel', function(hooks) {
  setupApplicationTest(hooks);

  test('visiting /', async function(assert) {
    await visit('/');

    assert.equal(currentURL(), '/');
    assert.dom('nav').exists();
    assert.dom('h1').hasText('What is travel? And what is a traveler?');
    assert.dom('p').exists();

    assert.dom('a.button').hasText('Discover the world!');
    await click('a.button');

    assert.equal(currentURL(), '/countries');
  });

  test('visiting /countries', async function(assert) {
    await visit('/countries');

    assert.equal(currentURL(), '/countries');
    assert.dom('nav').exists();
    assert.dom('h2').hasText('... are you? Where you want to be...');
  });

  test('visiting /contact', async function(assert) {
    await visit('/contact');

    assert.equal(currentURL(), '/contact');
    assert.dom('h2').hasText('Contact Us');
    assert.dom('a.button').hasText('Expande your mind');
    await click('a.button');

    assert.equal(currentURL(), '/countries');
  });

  test('navigating using the nav-bar', async function(assert) {
    await visit('/');

    assert.dom('nav').exists();
    assert.dom('nav a.navbar-brand').hasText('Travel all your tyme!')
    assert.dom('nav a.menu-country').hasText('Countries')
    assert.dom('nav a.menu-district').hasText('Districts')
    assert.dom('nav a.menu-city').hasText('Cities')
    assert.dom('nav a.menu-index').hasText('Home (current)')
    // assert.dom('nav a.menu-random').hasText('Random Choose');
    assert.dom('nav a.menu-contact').hasText('Contact');

    // await click('.menu-brand');
    // assert.equal(currentURL(), '/');

    await click('nav a.menu-country');
    assert.equal(currentURL(), '/countries');

    await click('nav a.menu-district');
    assert.equal(currentURL(), '/districts');

    await click('nav a.menu-city');
    assert.equal(currentURL(), '/cities');

    await click('nav a.menu-contact');
    assert.equal(currentURL(), '/contact');

    await click('nav a.menu-index');
    assert.equal(currentURL(), '/');

    await click('nav a.menu-contact');
    assert.equal(currentURL(), '/contact');
  });
});
