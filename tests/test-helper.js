import Application from 'where-to-travel/app';
import config from 'where-to-travel/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';

setApplication(Application.create(config.APP));

start();
