import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | item-list', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders the content inside a item-list with list', async function(assert) {
    this.setProperties({
        scientists: [
          "Marie Curie",
          "Mae Jemison",
          "Albert Hofmann"
        ],
      });

    await render(hbs`<ItemList @title="List of Scientists" />`);

    assert.dom('h2').hasText('List of Scientists');
    // assert.dom('.people-list').exists();
    // assert.dom('li').exists();
    // assert.dom('button').exists();
  });
});
