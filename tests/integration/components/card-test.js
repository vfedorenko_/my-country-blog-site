import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

module('Integration | Component | card', function(hooks) {
  setupRenderingTest(hooks);

  test('it renders information about a card component', async function(assert) {
    this.setProperties({
      card: {
        // bgImage
        backgroundImage: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Piazza_Venezia_-_Il_Vittoriano.jpg/1280px-Piazza_Venezia_-_Il_Vittoriano.jpg',
        flag: 'https://upload.wikimedia.org/wikipedia/en/0/03/Flag_of_Italy.svg',
        herb: 'https://upload.wikimedia.org/wikipedia/commons/0/00/Emblem_of_Italy.svg',
        // country_name, country_name_ln
        country: "L'Italia",
        area: 301340,
        population: 6031711,
        capital: 'Rome',
        officialLang: 'Italian',
        // info
        short: 'Italy and San Marino. Italy has a territorial enclave in Switzerland (Campione) and a maritime exclave in Tunisian waters (Lampedusa). With around 60 million inhabitants European Union.',
        // infoShort
        long: "L'Italia è uno Stato situato nell'Europa meridionale, il cui territorio coincide in gran parte con l'omonima regione geografica. L'Italia è una repubblica ultramillenaria, l'Italia vanta insieme alla Cina il maggior numero di siti dichiarati patrimonio dell'umanità dall'UNESCO[17].",
        cities: 24,
        places: 124,
      }
    });

    await render(hbs`<Card @item={{this.card}} />`);

    // Template block usage:
    assert.dom('section').hasClass('card-body');
    assert.dom('section header h1').hasText("L'Italia");
    assert.dom('section .card-subbody .left').includesText('Official Language: Italian');
    assert.dom('section .card-subbody .right').includesText('and San Marino');
    assert.dom('section footer').includesText('Area: 301340');
    assert.dom('section footer').includesText('Population: 6031711');
    assert.dom('section header img').exists();
  });
});
