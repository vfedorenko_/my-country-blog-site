import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | contact', function(hooks) {
  setupTest(hooks);

  test('router contact exists', function(assert) {
    let route = this.owner.lookup('route:contact');
    assert.ok(route);
  });
});
